from scipy.integrate import odeint
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import pickle
import os
from scipy.stats import linregress
from scipy.special import gamma
from scipy.optimize import curve_fit
import warnings

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

class Fluorescence(object):
    def __init__(self,B01=2, B10=2, A10=5e-4, Q0=1e-1, Q1=1e-2,time=None, y_init=(1e20,0)):
        """
B01*I*y[0] ... rate of absorption of laser photons
B10*I*y[0] ... rate of laser-stimulated emission 
Q0 ... non radiative transitions to level 0
Q1 ...  non radiative transitions from level 1
"""
        self.B01 = B01
        self.B10 = B10
        self.A10 = A10
        self.Q0 = Q0
        self.Q1 = Q1
        self.time = time
        if self.time is None:
            self.time = np.linspace(0, 600, 3000)
        self.y_init = np.array(y_init)
        self.result = None
        self.info = None
        self.Ifactor = None



    def deriv(self, y, t, I, Ifactor):
        """
y[0] ... ground state
y[1] ... laser excited state
"""
        return np.array([-self.B01*I(t, Ifactor) * y[0] + (self.B10*I(t, Ifactor) + self.A10)*y[1] 
                         + self.Q0*(self.y_init[0]-y[0]),
                         self.B01*I(t, Ifactor) * y[0] - 
                         (self.B10*I(t, Ifactor) + self.A10 + self.Q1)*y[1]])
    @staticmethod
    def laser_pulse(t, Ifactor, laser_b=12.6, laser_c=2.44):
        """Calculate the laser pulse in the form Ifactor*(t)**laser_b * np.exp(-laser_c*t) / norm, 
        where norm is calculated such, that integral from 0 to infty of the laser pulse equals Ifactor. 

"""
        if laser_b is None and laser_c is None:
            laser_b = 12.6
            laser_c = 2.44
            integrated = 11918.0082313
        else:
            integrated = laser_c**(-laser_b-1)*gamma(laser_b+1)
        ret = Ifactor*(t)**laser_b * np.exp(-laser_c*t) / integrated
        return ret

    def integrate(self,Ifactor):
        self.Ifactor = Ifactor
        func = lambda y, t: self.deriv(y, t, self.laser_pulse, Ifactor)
        self.result, self.info = odeint(func, self.y_init, self.time, full_output = True)
        return self.result, self.info

    def print_info(self):
        for k, v in self.info.items():
            print (k, '-->', v)

    
    def total_fluorescence(self):
        return np.trapz(self.result[:,1],self.time)

    def save(self, filename):
        with open(filename, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load(filename):
        with open(filename, 'rb') as inp:
            return_val = pickle.load(inp)
        return return_val  


class FourLevels(Fluorescence):
    """
y[0] ... lower state of the laser-induced transition
y[1] ... higher state of the laser-induced transition
y[2] ... sum of all other rotational levels of the laser-excited vibronic state
y[3] ... sum of all other rotational levels of the lower vibronic state

B01*I ... rate of absorption of laser photons
B10*I ... rate of laser-stimulated emission 
Q13 ... rate of non-radiative transitions from level 1 to level 3
Qret0*(fn0*y[3] - y[0]) .. rotational energy transfer brings the system to thermal equilibrium
"""
    def __init__(self, Q13=1e-2, QRET0=5e-1, QRET1=5e-1, fn0=8e-2,
                 fn1=5e-2, Q20=None, A20=None, Q23=1e-2, A23=5e-4,
                 B01=2, B10=2, A10=None, Q10=None, A13=5e-4,
                 time=None, y_init=(8e18,0,0,1e20),):
        Fluorescence.__init__(self,B01=B01, B10=B10, A10=A10, Q0=QRET0, Q1=Q13,time=time, y_init=(y_init[0], y_init[-1]))
        self.Q13=Q13
        self.QRET0=QRET0
        self.QRET1=QRET1
        self.fn0=fn0
        self.fn1=fn1
        if A20 is None:
            self.A20=A23/5. #realistic ratio for OH rotational lines
        else:
            self.A20 = A20
        if Q20 is None:
            self.Q20=Q23*fn0
        else:
            self.Q20 = Q20
        self.A23=A23
        self.Q23=Q23
        if Q10 is None:
            self.Q10=Q13*fn0
        else:
            self.Q10 = Q10
        if A10 is None:
            self.A10=A13/5. #realistic ratio
        else:
            self.A10 = A10
        self.A13=A13
        self.y_init=y_init


    def from_dict(self, **kwargs):
        self.Q13 = kwargs.pop('Q13', self.Q13)
        self.Q10 = kwargs.pop('Q10', self.Q10)
        self.QRET0 = kwargs.pop('QRET0', self.QRET0)
        self.A10 = kwargs.pop('A10', self.A10)
        self.A13 = kwargs.pop('A13', self.A13)
        self.QRET1 = kwargs.pop('QRET1', self.QRET1)
        self.A20 = kwargs.pop('A20', self.A20)
        self.A23 = kwargs.pop('A23', self.A23)
        self.Q20 = kwargs.pop('Q20', self.Q20)
        self.Q23 = kwargs.pop('Q23', self.Q23)
        self.B10 = kwargs.pop('B10', self.B10)
        self.B01 = kwargs.pop('B01', self.B01)
        
    def deriv(self, y, t, I, Ifactor):
        return np.array([-self.B01*I(t, Ifactor)*y[0] +  
                         (self.B10*I(t, Ifactor) + self.A10 + self.Q10)*y[1] +
                         (self.Q20 + self.A20)*y[2] +
                         self.QRET0*(self.fn0*y[3] - (1-self.fn0)*y[0]),

                         self.B01*I(t, Ifactor) * y[0] - 
                         (self.B10*I(t, Ifactor) + self.A10 + self.Q10 + self.Q13 + self.A13)*y[1]+ 
                         self.QRET1*(y[2]*self.fn1 - (1-self.fn1)*y[1]),

                         self.QRET1*((1-self.fn1)*y[1] - y[2] * self.fn1) - 
                         (self.A20 + self.A23 + self.Q20 + self.Q23)*y[2],

                         self.QRET0*((1-self.fn0)*y[0] - self.fn0*y[3]) + 
                         (self.A23 + self.Q23)*y[2] + (self.Q13+self.A13)*y[1]])

    def total_fluorescence(self):
        return np.trapz((self.A10+self.A13)*self.result[:,1] + 
                        (self.A20+self.A23)*self.result[:,2], self.time) 


class FluorescenceVsPower(object):
    def __init__(self, directory, Ifactors=None, params_dict={}, **kwargs):
        if os.path.exists(directory):
            msg = 'FluorescenceVsPower: directory ' + directory + ' exists! Consider removing it before continuing.'
            #warnings.warn(msg, Warning)
            raw_input(msg+"\nPress Enter to continue...")
        if not os.path.exists(directory):
            os.makedirs(directory)
        self.F = FourLevels(**kwargs)
        self.F.from_dict(params_dict)
        self.models = {}
        if Ifactors is None:
            Ifactors = np.linspace(0, 1e-4, 50)
        for i,Ifactor in enumerate(Ifactors):
            self.F.integrate(Ifactor)
            Fname = directory+'/'+str(i)+'.fl'
            self.F.save(Fname)
            self.models[Ifactor]=Fname
        self.result = {}
        self.p1factor=1
        self.p2factor=2
        self.p0 = 0
        self.p1 = 0
        self.p2 = 0
        self.fit_range = (0,0)
        self.A = self.F.A20 + self.F.A23 + self.F.fn1*(self.F.A10 + self.F.A13)
        self.Q = self.F.Q20 + self.F.Q23 + self.F.fn1*(self.F.Q10 + self.F.Q13)
        self.N = self.F.y_init[0]+self.F.y_init[3]
        self.Babs = self.F.B01 * self.F.fn0
        self.Bstim = self.F.B10*self.F.fn1
        self.B = self.Babs + self.Bstim
        self.T = np.sqrt(12.6+1)/2.44

    def total_fluorescence_vs_pwr(self):
        out = np.empty((len(self.models),2))
        for i,Ifactor in enumerate(self.models):
            F = FourLevels.load(self.models[Ifactor])
            out[i,0] = F.Ifactor
            out[i,1] = F.total_fluorescence()
        return out

    def inverted(self, data):
        inverted = 1./data
        mask = np.ones(data[:,0].shape) == 1
        mask[np.isinf(inverted[:,0])] = False
        mask[inverted[:,0]<0] = False
        mask[np.isnan(inverted[:,0])] = False
        return inverted[mask,:]

    def fit_linearized(self, Imin=None, Imax=None):
        if Imin is None:
            Imin = 0
        if Imax is None:
            Imax = max(self.result.keys())
        toinvert = self.crop(Imin, Imax)
        inverted = self.inverted(toinvert)
        slope, intercept, r, p, stderr = linregress(inverted[:,0], inverted[:,1])
        self.result['alpha'] = 1/slope
        self.result['beta'] = intercept * self.result['alpha']
        self.result['fit_results'] = {'slope':slope, 'intercept': intercept, 'r':r, 'p':p, 'stderr':stderr}
        return self.result['alpha'], self.result['beta']

    def refit_linearized(self, Imin=None, Imax=None, M=1):
        if Imin is None:
            Imin = 0
        if Imax is None:
            Imax = max(self.result.keys())
        #fl_vs_pwr = self.total_fluorescence_vs_pwr()
        fl_vs_pwr = self.crop(Imin, Imax)
        mask_pwr = self.result['beta']*fl_vs_pwr[:,0] < M
        inverted = self.inverted(fl_vs_pwr[mask_pwr,:])
        slope, intercept, r, p, stderr = linregress(inverted[:,0], inverted[:,1])
        self.result['alpha'] = 1/slope
        self.result['beta'] = intercept * self.result['alpha']
        self.result['fit_results'] = {'slope':slope, 'intercept': intercept, 'r':r, 'p':p, 'stderr':stderr}
        return self.result['alpha'], self.result['beta']

    def show_fit_linearize(self, save_to_file=None):
        fl_vs_pwr = self.total_fluorescence_vs_pwr()
        maxI = max(fl_vs_pwr[:,0])
        xfit = np.linspace(0, maxI, 100)
        yfit = self.func_alphabeta(xfit)
        fig, ax = plt.subplots(1, 1)
        ax.plot(fl_vs_pwr[:,0], fl_vs_pwr[:,1], '+')
        ax.plot(xfit, yfit)
        if save_to_file:
            fig.savefig(save_to_file)
        else:
            plt.show()
        return fig

    def func_alphabeta(self, pwr):
        return self.result['alpha'] * pwr / (1 + self.result['beta']*pwr)

    @staticmethod
    def func_3params(El,p0,p1,p2):
        return p0 * (El - p1*El**2 / (p1*El + p2)*((np.exp(-(p1*El + p2)) - 1)/(p1*El + p2) + 1))

    # def func_3params(self, El,p0,p1,p2):
    #     return p0 * (El - p1*El**2 / (p1*El + p2)*((np.exp(-(p1*El + p2)) - 1)/(p1*El + p2) + 1))


    def func_2params(self,El,p0,p2):
        self.p1 = self.F.B10*self.F.fn1 + self.F.B01 * self.F.fn0
        self.p1 *= self.p1factor
        return p0 * (El - self.p1*El**2 / (self.p1*El + p2)*((np.exp(-(self.p1*El + p2)) - 1)/(self.p1*El + p2) + 1))

    def func_1param(self,El,p0):
        self.p1 = self.F.B10*self.F.fn1 + self.F.B01 * self.F.fn0
        self.p1 *= self.p1factor
        self.p2  = (self.A+self.Q)*self.T * self.p2factor 
        return p0 * (El - self.p1*El**2 / (self.p1*El + self.p2)*((np.exp(-(self.p1*El + self.p2)) - 1)/(self.p1*El + self.p2) + 1))

    def crop(self,start,stop):
        tot = self.total_fluorescence_vs_pwr()
        tot = tot[tot[:,0].argsort()]
        idxmin = find_nearest(tot[:,0], start)
        idxmax = find_nearest(tot[:,0],stop)
        return tot[idxmin:idxmax,:]
        
    
    def fit(self, Imin=None, Imax=None, method='3params', Tfactor=2):

        if Imin is None:
            Imin = 0
        if Imax is None:
            Imax = max(self.result.keys())
        dep = self.crop(Imin, Imax)
        if method == '3params':
            #fun = self.func_3params
            fun = FluorescenceVsPower.func_3params
            p0 = (self.A/(self.A+self.Q)*self.N*self.Babs, self.B, self.T*(self.A+self.Q)*self.p2factor)
            popt, pcov = curve_fit(fun, dep[:,0], dep[:,1], p0)
            self.p0, self.p1, self.p2 = popt
        elif method == '2params':
            fun = self.func_2params
            p0 = (self.A/self.A+self.Q*self.N*self.Babs, self.T*(self.A+self.Q))
            popt, pcov = curve_fit(fun, dep[:,0], dep[:,1], p0)
            self.p0, self.p2 = popt
        elif method == '1param':
            fun = self.func_1param
            p0 = (self.A/(self.A+self.Q)*self.N*self.Babs)
            popt, pcov = curve_fit(fun, dep[:,0], dep[:,1], p0)
            self.p0 = popt
        else:
            return None
        return popt, pcov

    def show_fit(self, save_to_file=None):
        fl_vs_pwr = self.total_fluorescence_vs_pwr()
        maxI = max(fl_vs_pwr[:,0])
        xfit = np.linspace(0, maxI, 100)
        yfit = self.func_3params(xfit,self.p0,self.p1,self.p2)
        fig, ax = plt.subplots(1, 1)
        ax.plot(fl_vs_pwr[:,0], fl_vs_pwr[:,1], '+')
        ax.plot(xfit, yfit)
        if save_to_file:
            fig.savefig(save_to_file)
        else:
            plt.show()
        return fig
        
        

    def reset_params(self):
        self.p0=self.A/(self.A+self.Q)*self.N*self.Bstim 
        self.p1=self.B 
        self.p2=self.T*(self.A+self.Q)
        return

        


# dirname = 'testdir'
# fvp = FluorescenceVsPower(dirname)
# fvp.fit()
# fvp.show_fit()
# fvp.refit()
# fvp.show_fit()



# dirname = 'model_fast_repopulation'
# F = Fluorescence(Q0=2)
# numpoints=3000
# Is = np.linspace(0,1e-3,numpoints)
# res = np.zeros(numpoints)
# pbar = progressbar.ProgressBar(numpoints)
# for i,Int in enumerate(Is):
#     F.integrate(Int)
#     res[i] = F.total_florescence()
#     F.save(dirname + '/'+str(i)+'.fl')
#     pbar.animate(i+1)
# plt.plot(Is, res, '+')
# plt.show()


# dirname = '4L_model'
# F = FourLevels()
# numpoints=3000
# Is = np.linspace(0,1e-4,numpoints)
# res = np.zeros(numpoints)
# pbar = progressbar.ProgressBar(numpoints)
# for i,Int in enumerate(Is):
#     F.integrate(Int)
#     res[i] = F.total_florescence()
#     F.save(dirname + '/'+str(i)+'.fl')
#     pbar.animate(i+1)
# plt.plot(Is, res)
# plt.show()


# numpoints=3000
# Is = np.linspace(0,1e-2,numpoints)
# res = np.zeros(numpoints)
# pbar = progressbar.ProgressBar(numpoints)
# for i in range(numpoints):
#     model = FourLevels.load(dirname+'/'+str(i)+'.fl')
#     res[i] = model.total_florescence()
#     pbar.animate(i+1)
# np.save(dirname+'/res.npy', res)
# np.save(dirname+'/Is.npy', Is)

    
# for i in range(3000/5):
#     model = FourLevels.load(dirname+'/'+str(i)+'.fl')
#     plt.plot(model.time, model.result[:,0])
#     plt.plot(model.time, model.result[:,1])
#     plt.plot(model.time, model.result[:,2])
#     plt.title(str(model.Ifactor))
#     plt.draw()
#     plt.pause(0.01)
#     plt.gca().clear()

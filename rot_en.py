import numpy as np

kB = 1.380e-23
def E_N_Sigma(N, manifold, **kwargs):
      """
Calculate rotational energy in J of state with rotational number N in A 2
Sigma state of the OH molecule according to 

Dieke, G. H., and H. M. Crosswhite. "The ultraviolet bands of OH fundamental data." 
Journal of Quantitative Spectroscopy and Radiative Transfer 2.2 (1962): 97-199.

Manifold is::

  1 for J = N + 1/2

  2 for J = N - 1/2

By default calculates for v = 0. 

`B`, `D` and `R` parameters (see the mentioned paper) can be accessed as kwargs of the same name. 

Example::

  E_R = rot_en.E_N_Sigma(4, 1, B = 3e-22, D = 4e-26. R = 2.2e-24)

      """
      if not isinstance(N,int):
            print "E_N_Sigma Warning! N must be an integer!" 
            return 0
      
      B = kwargs.pop('B',3.3692e-22)
      D = kwargs.pop('D',4.0523e-26)
      R = kwargs.pop('R',2.2288e-24)
      e_N = B*N*(N+1) - D*N**2*(N+1)**2;
      if (manifold == 1):
          e_N += R*(N+1/2)
      elif (manifold == 2):
          e_N -= R*(N+1/2)
      else:
          print "E_N_Sigma: Warning! manifold not recognized!"
      return e_N


def E_N_Pi(N, manifold, **kwargs):
    """
Calculate rotational energy of state with rotational number N in X 2
Pi  state of the OH molecule according to 

Dieke, G. H., and H. M. Crosswhite. "The ultraviolet bands of OH fundamental data." 
Journal of Quantitative Spectroscopy and Radiative Transfer 2.2 (1962): 97-199.

Manifold is::

  1 for J = N + 1/2

  2 for J = N - 1/2

By default calculates for v = 0. 

`B`, `D` and `a` parameters (see the mentioned paper) can be accessed as kwargs of the same name. 

Example::

  E_R = rot_en.E_N_Sigma(4, 1, B = 4e-22, D = 4e-26. a = -7.5)
    """
    if not N > 0: 
          print "E_N_Pi Warning! N must be greater than 0!"
          return 0
    if not isinstance( N, int):
          print "E_N_Pi Warning! N must be an integer!" 
          return 0
    B = kwargs.pop('B',3.6779e-22)
    D = kwargs.pop('D',3.7146e-26)
    a = kwargs.pop('a',-7.547)
    ret = 0
    if (manifold == 1):
        ret = B*((N+1)**2 - 1 - 0.5*np.sqrt(4*(N+1)**2 + a*(a-4))) - D*N**2*(N+1)**2
    elif (manifold == 2):
        ret = B*(N**2 - 1 + 0.5*np.sqrt(4*N**2 + a*(a-4))) - D*N**2*(N+1)**2
    else:
        print "E_N_Pi Warning! manifold not recognized!"
    return ret

def weight_sum_A(A,T):
    sum=0
    for i in range(0,3): #len(A)):
          #print "mean_A: len(A) = " + str(len(A))
          j = i + 1
          sum +=  (2*((j-1)+0.5)+1) * np.exp(-E_N_Sigma((j-1),1)/(kB*T)) * A[i,1]; #P1 
          if (j > 1):
                sum += (2*((j-1)-0.5)+1) * np.exp(-E_N_Sigma((j-1),2)/(kB*T)) * A[i,2]; #P2
          sum += (2*((j)+0.5)+1) * np.exp(-E_N_Sigma((j),1)/(kB*T)) * A[i,3]; #Q1
          sum += (2*((j)-0.5)+1) * np.exp(-E_N_Sigma((j),2)/(kB*T)) * A[i,4]; #Q2
          sum += (2*((j+1)+0.5)+1) * np.exp(-E_N_Sigma((j+1),1)/(kB*T)) * A[i,5]; #R1
          sum += (2*((j+1)-0.5)+1) * np.exp(-E_N_Sigma((j+1),2)/(kB*T)) * A[i,6]; #R2
          if (j>1):
                sum += (2*((j-2)+0.5)+1) * np.exp(-E_N_Sigma((j-2),1)/(kB*T)) * A[i,7]; #O12
          sum += (2*((j)+0.5)+1) * np.exp(-E_N_Sigma((j),1)/(kB*T)) * A[i,8]; #Q12
          sum += (2*((j-1)+0.5)+1) * np.exp(-E_N_Sigma((j-1),1)/(kB*T)) * A[i,9]; #P12
          sum += (2*((j+1)-0.5)+1) * np.exp(-E_N_Sigma((j+1),2)/(kB*T)) * A[i,10]; #R21
          sum += (2*((j)-0.5)+1) * np.exp(-E_N_Sigma((j),2)/(kB*T)) * A[i,11]; #Q21
          if (j<len(A)):
                sum += (2*((j+2)-0.5)+1) * np.exp(-E_N_Sigma((j+2),2)/(kB*T)) * A[i,12]; #S21
    return sum;


def rot_frac_Pi(N,manifold,T, highest_N=40):
      """
Calculate the Boltzmann fraction of the given rotational state of the
X 2 Pi state of OH molecule for temperature T (in K). By default
calculates with N up to 39. Lambda-doubling is not taken into account
during the calculations, but affects the result. For example,
`rot_frac_Pi(4,1,700)` returns the Boltzmann fraction of the state
with N = 4, J = 9/2 dvided by 2, as preferable for Pi <-> Sigma transitions 
due to the parity selection rule. 

Manifold is::

  1 for J = N + 1/2

  2 for J = N - 1/2
      """
      if N == 0:
            return 0
      f1 = 0
      f2 = 0
      ret = 0
      #F1 manifold
      for n in range(1,highest_N):
            # 2 for lambda-doubling, (2J+1) for rotation
            f1 += 2*(2*(n+0.5) + 1)*np.exp(-E_N_Pi(n,1)/(kB*T))
      #F2 manifold
      for n in range(1,highest_N):
            # 2 for lambda-doubling, (2J+1) for rotation
            f2 += 2*(2*(n-0.5)+1)*np.exp(-E_N_Pi(n,2)/(kB*T))
      if (manifold == 1):
            # lambda doubling factor not included due to the symmetry
            # selection rule + <-> -
            ret = (2*(N+0.5) + 1)*np.exp(-E_N_Pi(N,1)/(kB*T))/(f1+f2)
      elif (manifold == 2):
            ret = (2*(N-0.5) + 1)*np.exp(-E_N_Pi(N,2)/(kB*T))/(f1+f2)
      else:
            printf("rot_frac_Pi() warning! manifold not recognized!\n")
            ret = 0
      return ret 

def rot_frac(N,manifold,T):
      print "Warning! rot_frac() is deprecated. Use rot_frac_Pi() instead!"
      return rot_frac_Pi(N,manifold,T)

def rot_frac_Sigma(N,manifold,T,highest_N=40):
      """
Calculate the Boltzmann fraction of the given rotational state of the
A 2 Sigma state of OH molecule for temperature T (in K). By default
calculates with N up to 39.

Manifold is::

  1 for J = N + 1/2

  2 for J = N - 1/2
      """
      if manifold != 1 and manifold != 2:
            print "rot_frac_Pi() warning! manifold not recognized!"
            return 0

      f1 = 0
      f2 = 0
      #F1 manifold
      for n in range(0,highest_N):
            f1 += (2*(n+0.5) + 1)*np.exp(-E_N_Sigma(n,1)/(kB*T))
      #F2 manifold
      for n in range(0,highest_N):
            f2 += (2*(n-0.5) + 1)*np.exp(-E_N_Sigma(n,2)/(kB*T))
      if manifold == 1:
            ret = (2*(N+0.5)+1)*np.exp(-E_N_Sigma(N,1)/(kB*T))/(f1+f2)
      elif manifold == 2:
            ret = (2*(N-0.5)+1)*np.exp(-E_N_Sigma(N,2)/(kB*T))/(f1+f2)
      return ret


def mean_A(A, T):
      """
Calculate the mean emission coefficient for given temperature
according to formulas (8) and (9) of

Vorac, Jan, et al. "Measurement of hydroxyl radical (OH) concentration in an argon RF 
plasma jet by laser-induced fluorescence." 
Plasma Sources Science and Technology 22.2 (2013): 025016.

Args:
  **A**: table of emission coeffcients as exported by LIFBASE for Hund's case (b) setting. 

  **T**: temperature in K
      """
      #return weight_sum_A(A,T)/weight_sum_A(np.ones(np.shape(A)),T)
      sum=0
      for i in range(0,len(A)):
          #print "mean_A: len(A) = " + str(len(A))
          j = i + 1
          sum +=  rot_frac_Sigma(j-1,1,T) * A[i,1]; #P1 
          if (j > 1):
                sum += rot_frac_Sigma(j-1,2,T) *  A[i,2]; #P2
          sum += rot_frac_Sigma(j,1,T)  * A[i,3]; #Q1
          sum += rot_frac_Sigma(j,2,T)  * A[i,4]; #Q2
          sum += rot_frac_Sigma(j+1,1,T)  * A[i,5]; #R1
          sum += rot_frac_Sigma(j+1,2,T)  * A[i,6]; #R2
          if (j>1):
                sum += rot_frac_Sigma(j-2,1,T)  * A[i,7]; #O12
          sum += rot_frac_Sigma(j,1,T)  * A[i,8]; #Q12
          sum += rot_frac_Sigma(j-1,1,T)  * A[i,9]; #P12
          sum += rot_frac_Sigma(j+1,2,T)  * A[i,10]; #R21
          sum += rot_frac_Sigma(j,2,T)  * A[i,11]; #Q21
          if (j<len(A)):
                sum += rot_frac_Sigma(j+2,2,T)* np.exp(-E_N_Sigma((j+2),2)/(kB*T)) * A[i,12]; #S21
      return sum





